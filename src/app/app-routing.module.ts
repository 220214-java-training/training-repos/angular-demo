import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { PokeListComponent } from './components/poke-list/poke-list.component';

/**
 * This routes object defines which urls are mapped to which components.
 * The current URL determines the component, and that component is rendered underneath
 * the router-outlet element in the application.
 * 
 * Angular will check these routes IN ORDER. Order does matter.
 * The first matching url will be rendered.
 * 
 * If there are conflicts in the url, only the first component will be displayed.
 * 
 * With this in mind, it is often helpful to have a wildcard route
 * that redirects back to some sort of home page.
 * 
 * In case the user enters an incorrect URL, they will just be redirected
 * back to the homepage.
 */
const routes: Routes = [
  {
    path: 'dashboard', component: DashboardComponent,
  },
  {
    path: 'pokemon', component: PokeListComponent,
  },
  {
    path: 'login', component: LoginComponent,
  },
  {
    path: '**', redirectTo: 'dashboard'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
