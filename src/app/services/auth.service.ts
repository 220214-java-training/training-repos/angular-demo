import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  currentUser: null | User = null;

  constructor(private http: HttpClient) { }

  isLoggedIn(): boolean {
    return this.currentUser !== null;
  }

  isFinanceManager(): boolean {
    return this.currentUser?.role === 'FinanceManager';
  }

  login(username: string, password: string): Observable<User> {
    return this.http.post<User>("http://localhost:8080/login", {
        username,
        password
      }, {
        withCredentials: true
      }).pipe(
      tap((response) => this.currentUser = response)
    );
  }

  logout(): Observable<void> {
    return this.http.post<void>("http://localhost:8080/logout", undefined, {
      withCredentials: true
    }).pipe(
      tap(() => this.currentUser = null)
    );
  }
}

/**
 * Backend:
 * 
 * @PostMapping("/login")
 * public ResponseEntity<User> login(@RequestBody LoginRequest loginRequest, HttpSession session) {
 *   User user = this.userService.getByUsernameAndPassword(loginRequest.getUsername(), loginRequest.getPassword());
 *   if(user != null) {
 *     session.setAttribute("user", user);
 *   } else {
 *     // Return 400 Bad Request
 *   }
 * 
 *   return user;
 * }
 * 
 * 
 * In your other methods:
 * @PostMapping("/account")
 * public ResponseEntity<Account> createAccount(@RequestBody CreateAccountRequest createAccountRequest, HttpSession session) {
 *   if(session == null || session.getAttribute("user") == null) {
 *     // The user is not logged in, so return a 400 or similar
 *   }
 * }
 * 
 */