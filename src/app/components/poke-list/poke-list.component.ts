import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs';
import { Pokemon } from 'src/app/models/pokemon';
import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
  selector: 'app-poke-list',
  templateUrl: './poke-list.component.html',
  styleUrls: ['./poke-list.component.css']
})
export class PokeListComponent {

  search: string = '';
  pokemon: Pokemon[] = [];

  constructor(private pokemonService: PokemonService) { }

  fetchPokemon(): void {
    this.pokemonService.getPokemon(this.search).pipe(
      map((pokemon) => {
        pokemon.name = pokemon.name.toUpperCase();
        return pokemon;
      })
    ).subscribe((pokemon) => {
      this.pokemon.push(pokemon);
    });
  }

}
