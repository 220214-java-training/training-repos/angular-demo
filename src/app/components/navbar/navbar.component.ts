import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {

  constructor(private router: Router, private authService: AuthService) { }

  logout(): void {
    // Perform some logic, such as removing the logged in user's data
    console.log("The user has been logged out");
    this.authService.logout().subscribe();
    this.router.navigate(['dashboard']); // Navigate back to the login page or similar
  }

  isLoggedIn(): boolean {
    return this.authService.isLoggedIn();
  }
}
