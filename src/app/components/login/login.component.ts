import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) { }

  loginForm = this.formBuilder.group({
    username: '',
    password: ''
  });

  login(): void {
    const username: string = this.loginForm.value.username;
    const password: string = this.loginForm.value.password;

    this.authService.login(username, password).subscribe(
      (user) => {
        this.router.navigate(['dashboard']);
      }
    );

    this.loginForm.reset();
  }
}
